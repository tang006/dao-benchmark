package com.ibeetl.dao;

import com.ibeetl.dao.common.TestServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;

@SpringBootApplication
public class BeanchmarkApplication {

    @Autowired
    TestServiceInterface testService;

    @Value("${test.warmCount}")
    private int warmCount;
    @Value("${test.count}")
    private int testCount;

    @Value("${test.target}")
    private String target;


    private Map<String, Long> map = new HashMap<String, Long>();

    public static void main(String[] args) {
        SpringApplication.run(BeanchmarkApplication.class, args);

    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {
            test(warmCount, false);
            test(testCount, true);
            logFile(target);
           
        };
    }

    public void test(int time, boolean log) {
        testAdd(time, log);
        testUnique(time, log);
        testUpdateById(time, log);
        testPageQuery(time, log);
        testExample(time, log);
       

    }

    public void logFile(String target) throws IOException {
        String root = System.getProperty("user.dir") + File.separator + "result";
        new File(root).mkdirs();
        File file = new File(root, target + ".txt");
        Properties ps = new Properties();
        for (Entry<String, Long> entry : map.entrySet()) {
            ps.put(entry.getKey(), String.valueOf(getTPS(entry.getValue())));
        }
        ps.store(new FileWriter(file), target + ",total=" + testCount+" tps:");
        System.out.println(ps);
    }
    
    private long getTPS(Long misc) {
        if(misc==0) {
            //有些测试部支持，返回0
            return 0;
        }
        return 1000*testCount/misc;
    }

    public void testAdd(int time, boolean log) {
        if (log) {
            start("testAdd");
        }
        for (int i = 0; i < time; i++) {
            testService.testAdd();
        }
        if (log) {
            end("testAdd");
        }
    }

    public void testUnique(int time, boolean log) {
        if (log) {
            start("testUnique");
        }
        for (int i = 0; i < time; i++) {
            testService.testUnique();
        }
        if (log) {
            end("testUnique");
        }
    }

    public void testUpdateById(int time, boolean log) {
        if (log) {
            start("testUpdateById");
        }
        for (int i = 0; i < time; i++) {
            testService.testUpdateById();
        }
        if (log) {
            end("testUpdateById");
        }
    }

    public void testPageQuery(int time, boolean log) {
        if (log) {
            start("testPageQuery");
        }
        for (int i = 0; i < time; i++) {
            testService.testPageQuery();
        }
        if (log) {
            end("testPageQuery");
        }
    }

    public void testExample(int time, boolean log) {
        if (log) {
            start("testExample");
        }
        for (int i = 0; i < time; i++) {
            testService.testExampleQuery();
        }
        if (log) {
            end("testExample");
        }
    }

    public void start(String key) {
        map.put(key, System.currentTimeMillis());
    }

    public void end(String key) {
        Long start = map.get(key);
        Long end = System.currentTimeMillis();
        map.put(key, end - start);
    }

}

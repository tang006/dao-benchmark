# dao-benchmark

#### 项目介绍
dao 性能测试，包含jpa，mybatis，beetlsql等Dao工具，测试了插入，修改，查询，翻页查询等常用操作性能，以此来优化BeetlSQL性能

#### 运行本例子

* git clone https://gitee.com/xiandafu/dao-benchmark
*  mvn clean package
*  java -jar -Dtest.target=jpa  target/dao-0.0.1-SNAPSHOT.jar
*  测试目标可更换为jpa,beetlsql,mybatis,jdbc,think,anima
*  在result目录检测测试文本结果



#### 测试过程说明
采用H2内存数据库，如下配置

	#db config
	spring.datasource.username=sa
	spring.datasource.password=
	spring.datasource.driver-class-name=org.h2.Driver
	spring.datasource.url=jdbc:h2:mem:dbtest


	spring.datasource.schema=classpath:db/schema.sql
	#spring.datasource.data=classpath:db/data.sql 
	
	# test config
	test.target = beetlsql
	test.warmCount=2
	test.count=1000
	
	#beetlsql config
	beetlsql.mutiple.datasource=datasource
	beetlsql.ds.datasource.basePackage=com.ibeetl.dao.beetlsql
	beetl-beetlsql.dev=false
	beetl.enabled=false

BeanchmarkApplication 是程序入口，启动后即可做测试

	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {
			test(warmCount,false);
			test(testCount,true);
		};
	}

可以设定预热次数，以及是否记录日志。

测试完毕，自动打印每个测试的方法并记录到文件系统里,位于程序运行的result目录，每个文件内容如下

```
#beetlsql,total: 1000
#Tue May 22 22:44:08 CST 2018
testAdd=194
testExample=45
testPageQuery=88
testUpdateById=93
testUnique=80


```

#### 测试场景

* testAdd： 向数据库插入一条用户记录
* testUpdateById  根据主键更新以一条用户记录
* testPageQuery  翻页查询
* testUnique 根据主键查询用户记录
* testExample 模板类查询

#### 增加新的Dao

参考TestServiceConfig，提供一个TestServiceInterface接口实现

```
@Configuration
public class DataSourceConfig {
	
	
	@Bean
	@ConditionalOnProperty(name = "test.target", havingValue = "beetlsql")
	public TestServiceInterface getBeetlSqlTestService() {
		return new BeetlSqlPerformaceTestService();
	}
	@Bean
	@ConditionalOnProperty(name = "test.target", havingValue = "jpa")
	public TestServiceInterface getJpaTestService() {
		return new JpaPerformaceTestService();
	}
	
	
}
```

#### 测试截图



![](performance.png)



#### 测试结果说明

测试结果，JDBC遥遥领先，BeetlSQL 紧随其后，对于全功能Dao框架来说，BeetlSQL表现无疑是最好的，排名如下

* 1. beetlsql
* 2. mybatis
* 3. JPA
* 4. 欢迎加入你的Dao...

ThinkJDBC 性能表现也非常好，不过很多功能是在DBUtils上的封装，最接近Raw-JDBC功能，因此不再列出


#### TODO

* ORM测试，简单场景下一对多的测试
* EBean测试
* 测试结果自动生成图片

